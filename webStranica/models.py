from dynamorm import DynaModel
from marshmallow import fields, Schema
from django.conf import settings


class PyComData(DynaModel):
	class Table:
		resource_kwargs = {
			'aws_access_key_id' : settings.AWS_ACCESS_KEY_ID,
			'aws_secret_access_key' : settings.AWS_SECRET_ACCESS_KEY,
			'region_name' : settings.AWS_REGION
		}

		name = 'pycom_data'
		hash_key = 'sample_time'
	
	class Schema(Schema):
		payload = fields.Mapping()
		device_id = fields.Number()
		topic = fields.String()
		sample_time = fields.Number()