import paho.mqtt.publish as publish
import ssl
import json

awshost = "a2hayhpqwclwki-ats.iot.us-west-2.amazonaws.com"
awsport = 8883
caPath="./webStranica/mqtt/root-CA.crt"
certPath="./webStranica/mqtt/DjangoApp.cert.pem"
keyPath="./webStranica/mqtt/DjangoApp.private.key"

clientId = "DjangoPublish"
tlsDict = {'ca_certs': caPath, 'certfile': certPath, 'keyfile': keyPath,
           'tls_version': ssl.PROTOCOL_TLSv1_2, 'ciphers': None}

#tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)


def publishSingle(topic="general", message="msg", client=clientId):
    mesag = json.dumps(message)
    publish.single(topic, mesag, qos=1, hostname=awshost,
                   port=awsport, client_id=clientId, keepalive=60, tls=tlsDict)


def publishMultiple(msgs):
    publish.multiple(msgs, hostname=awshost,
                   port=awsport, client_id=clientId, keepalive=60, tls=tlsDict)

#msgs = [{'topic': "paho/test/multiple", 'payload': "multiple 1"},
#        ("paho/test/multiple", "multiple 2", 0, False)]

#publishSingle("device/temperatura/data", "23")

#message = {}
#message['message'] = args.message
#message['sequence'] = loopCount
#messageJson = json.dumps(message)
