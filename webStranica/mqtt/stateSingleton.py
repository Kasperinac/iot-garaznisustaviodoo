#singletonModule
vrata = 0
vrata_time = None
svjetlo = 0
svjetlo_time = None

def setStates(vrataNew = None, vrata_timeNew = None, svjetloNew = None, svjetlo_timeNew = None):
    global vrata, vrata_time, svjetlo, svjetlo_time

    if vrataNew is not None:
        vrata = vrataNew
    
    if vrata_timeNew is not None:
        vrata_time = vrata_timeNew

    if svjetloNew is not None:
        svjetlo = svjetloNew

    if svjetlo_timeNew is not None:
        svjetlo_time = svjetlo_timeNew

def getStates():
    return vrata, vrata_time, svjetlo, svjetlo_time