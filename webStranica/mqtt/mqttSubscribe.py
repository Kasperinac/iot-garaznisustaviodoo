import paho.mqtt.client as mqtt
from . import mqttPublish, stateSingleton
import time
import ssl
import json

def on_connect(client, userdata, flags, rc):
    client.subscribe("#" , 1 )

def on_message(client, userdata, msg):
    print("topic: "+msg.topic)
    print("payload: "+str(msg.payload))

def on_message_Temperatura(client, userdata, msg):
    pass

def on_message_PirSenzor(client, userdata, msg):
    global vrata, vrata_time, svjetlo, svjetlo_time
    jsonTemp = json.loads(msg.payload.decode('utf-8'))
    print(jsonTemp)
    mqttPublish.publishSingle("command/light",{'message':1}) #upali svjetlo
    mqttPublish.publishSingle("command/door", {'message':1}) #otvori vrata
    stateSingleton.setStates(1,int(time.time()*1000),1,int(time.time()*1000))

def on_message_Status(client, userdata, msg):
    jsonStatus = json.loads(msg.payload.decode('utf-8'))
    topic = msg.topic
    print(topic)
    if topic == 'state/garage':
        stateSingleton.setStates(vrataNew = jsonStatus['message'], vrata_timeNew= int(time.time()*1000))
    elif topic == 'state/light':
        stateSingleton.setStates(svjetloNew= jsonStatus['message'], svjetlo_timeNew= int(time.time()*1000))
    print(jsonStatus)


clientId = "DjangoSubscribe"

mqttClient = mqtt.Client(client_id = clientId)
mqttClient.on_connect = on_connect
mqttClient.on_message = on_message
mqttClient.message_callback_add("sensor/temperature", on_message_Temperatura)
mqttClient.message_callback_add("sensor/pir/+", on_message_PirSenzor)
mqttClient.message_callback_add("state/+", on_message_Status)

awshost = "a2hayhpqwclwki-ats.iot.us-west-2.amazonaws.com"
awsport = 8883

#caPath = "C:/Users/kaspe/Desktop/IoTProjekt/GarazniSustaviDoo/webStranica/mqtt/root-CA.crt"
#certPath = "C:/Users/kaspe/Desktop/IoTProjekt/GarazniSustaviDoo/webStranica/mqtt/DjangoApp.cert.pem"
#keyPath = "C:/Users/kaspe/Desktop/IoTProjekt/GarazniSustaviDoo/webStranica/mqtt/DjangoApp.private.key"
caPath="./webStranica/mqtt/root-CA.crt"
certPath="./webStranica/mqtt/DjangoApp.cert.pem"
keyPath="./webStranica/mqtt/DjangoApp.private.key"

mqttClient.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)

mqttClient.connect(awshost, awsport, keepalive=60)

mqttClient.loop_start()
