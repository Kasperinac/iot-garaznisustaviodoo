from webStranica.models import PyComData
from datetime import datetime
import time
from django.http import JsonResponse, HttpResponse
from .mqtt import mqttPublish, stateSingleton
import json

pycomDataSchema = PyComData.Schema()

#sve iz baze o temperaturi
def temperatureData(request):
    tempData = PyComData.scan(topic__begins_with='sensor/temperature')
    lista = list(tempData)
    nes = list(map(lambda jen : pycomDataSchema.dump(jen), lista))

    return JsonResponse({'data':nes})

#sve iz baze o pir senzorima (oba)
def pirData(request):
    tempData = PyComData.scan(topic__begins_with='sensor/pir')
    lista = list(tempData)
    nes = list(map(lambda jen : pycomDataSchema.dump(jen), lista))

    return JsonResponse({'data':nes})

#sve iz baze o vlazi
def humidityData(request):
    humData = PyComData.scan(topic__begins_with='sensor/humidity')
    lista = list(humData)
    nes = list(map(lambda jen : pycomDataSchema.dump(jen), lista))

    return JsonResponse({'data':nes})

#sve iz baze o garazi
def garageData(request):
    garState = PyComData.scan(topic__begins_with='state/garage')
    lista = list(garState)
    nes = list(map(lambda jen : pycomDataSchema.dump(jen), lista))

    return JsonResponse({'data':nes})

#sve iz baze o svjetlu
def lightData(reqeust):
    garState = PyComData.scan(topic__begins_with='state/light')
    lista = list(garState)
    nes = list(map(lambda jen : pycomDataSchema.dump(jen), lista))

    return JsonResponse({'data':nes})

#trenutno stanje i vrijeme kada se zadnje promijenilo i ua garazu i za svjetlo
def currentState(reqeust):
    var = stateSingleton.getStates()
    data = {'door':var[0], 'door_time':var[1], 'light':var[2], 'light_time':var[3]}
    return JsonResponse(data)

#endpoint za slanje naredbi otvaraj/zatvaraj value = 1 ili 0; device = door ili light
def sendCommand(request):
    device = ""
    value = ""
    data = json.loads(request.body)
    print(data)
    if 'value' in data:
        value = int(data['value'])
    if 'device' in data:
        device = data['device']
    topic = ""
    if device == 'door':
        topic = "command/door"
        stateSingleton.setStates(vrataNew=value, vrata_timeNew= int(time.time()*1000))
    elif device == 'light':
        topic = "command/light"
        stateSingleton.setStates(svjetloNew=value, svjetlo_timeNew= int(time.time()*1000))
    
    #stateSingleton.setStates(svjetloNew=0, svjetlo_timeNew= int(time.time()*1000))
    #topic = "command/light"
    #value = 0

    if topic:
        mqttPublish.publishSingle(topic,{'message':value})
        print("Publishao je ",topic, value)

    #mqttPublish.publishSingle("command/light",{'message':0})
    #mqttPublish.publishSingle("command/light",{'message':1})
    #mqttPublish.publishSingle("command/door",{'message':0})
    #mqttPublish.publishSingle("command/door",{'message':1})
    return HttpResponse()

#test
def bazaPraviTest(request):
    pycomData = PyComData.scan()
    lista = list(pycomData)
    nes = list(map(lambda jen : pycomDataSchema.dump(jen), lista))
    
    return JsonResponse({'data':nes})

#return PyComData.Schema().dump(nes)
#time = datetime.fromtimestamp(float(lista[0].sample_time)/1000)