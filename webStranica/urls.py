from django.urls import path

from . import views

urlpatterns = [
    path('temperatureData', views.temperatureData, name='temperatureData'),
    path('pirData', views.pirData, name='pirData'),
    path('humidityData', views.humidityData, name='humidityData'),
    path('garageData', views.garageData, name='garageData'),
    path('lightData', views.lightData, name='lightData'),
    path('currentState', views.currentState, name='currentState'),
    path('sendCommand', views.sendCommand, name = 'sendCommand'),
    path('bazaPraviTest', views.bazaPraviTest, name='bazaPraviTest'),
]